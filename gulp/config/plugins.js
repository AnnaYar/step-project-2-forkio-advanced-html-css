import replace from "gulp-replace"; //поиск и замена
// import gulpPlumber from "gulp-plumber"; //обработка ошибок
// import notify from "gulp-notify"; //сообщения
import newer from "gulp-newer"; //проверка обновления
import browserSync from "browser-sync";

export const plugins = {
    replace:replace,
    // gulpPlumber:gulpPlumber,
    // notify:notify,
    newer:newer,
    browserSync:browserSync,
}